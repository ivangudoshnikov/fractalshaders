#version 150 core

in vec4 in_Position;

uniform vec2 koeff,d,con;
uniform int iterations,pow;
uniform int type;

out vec4 pass_Position;

void main(void) {
	gl_Position = in_Position;
	pass_Position = in_Position;
}