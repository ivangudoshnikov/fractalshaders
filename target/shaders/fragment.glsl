#version 150 core

in vec4 pass_Position;

uniform vec2 koeff,d,con;
uniform int iterations,pow;
uniform int type;

out vec4 out_Color;

vec2 mult(vec2 a,vec2 b){  
  return vec2(a.x*b.x-a.y*b.y,a.x*b.y+a.y*b.x);
}

vec2 power(vec2 a, int n){
  vec2 result=vec2(1,0);
  for (int i=0; i<n; i++){
    result=mult(result,a);
  }
  return result;
}

void main(void) {
  bool paint = true;
  vec2 z;
  if (type==0){ //Mandelbrot
    z=con;  
    vec2 c=vec2(pass_Position.x*koeff.x+d.x,pass_Position.y*koeff.y+d.y);
    for (int i=-1; i<iterations; i++){
      if ((z.x>2)||(z.y>2)||(z.x*z.x+z.y*z.y>4)){
        paint=false;
        break;
      }   
      z=power(z,pow)+c;
    }    
  } else if (type==1){  //Julia
    z=vec2(pass_Position.x*koeff.x+d.x,pass_Position.y*koeff.y+d.y);  
    for (int i=0; i<iterations; i++){
      if ((z.x>2)||(z.y>2)||(z.x*z.x+z.y*z.y>4)){
        paint=false;
        break;
      }   
      z=power(z,pow)+con;
    }    
  }
  if (paint) {
    out_Color=vec4(1,1,1,1);
  } else {
    out_Color=vec4(0,0,0,1);
  }
  //mapping test:
  //if (distance(vec2(pass_Position.x*koeff.x+d.x,pass_Position.y*koeff.y+d.y),vec2(1f,1f))<0.01){
  //  out_Color=vec4(1,0,0,1);
  //}
}