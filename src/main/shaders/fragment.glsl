#version 150 core

in vec4 pass_Position;

//zoom and transpose of the camera, constant parameter(z0 for Mandelbrot, c for Julia)
uniform vec2 koeff,d,con;
//number of iterations, power  (z^n)
uniform int iterations,eq_pow;
//0 - Mandelbrot, 1 - Julia
uniform int type;
// 0 - black and white, 1 - gray, 2 - color 
uniform int color_scheme;


out vec4 out_Color;

vec4 inFracColor0(void){
    return vec4(1,1,1,1);
}

vec4 outFracColor0(int i){
    return vec4(0,0,0,1);
}

vec4 inFracColor1(void){
    return vec4(1,1,1,1);
}

vec4 outFracColor1(int i){
    return vec4(1,1,1,1)*sqrt(float(i)/iterations);
}

vec4 inFracColor2(void){
    return vec4(1,1,1,1);
}

vec4 outFracColor2(int i){
    //magneta, black, blue, yellow,red, cyan, lime
    const vec4 color_constants[7]=vec4[7](vec4(1,0,1,1),vec4(0,0,0,1),vec4(0,0,1,1),vec4(1,1,0,1),vec4(1,0,0,1),vec4(0,1,1,1),vec4(0,1,0,1));
    return color_constants[i%7]*0.65;
}

vec2 mult(vec2 a,vec2 b){  
  return vec2(a.x*b.x-a.y*b.y,a.x*b.y+a.y*b.x);
}

vec2 power(vec2 a, int n){
  vec2 result=vec2(1,0);
  for (int i=0; i<n; i++){
    result=mult(result,a);
  }
  return result;
}

void main(void) {
  bool paint = true;
  vec2 z;
  int i=0;
  if (type==0){ //Mandelbrot
    z=con;  
    vec2 c=vec2(pass_Position.x*koeff.x+d.x,pass_Position.y*koeff.y+d.y);
    
    while ((i<iterations)&& paint){
      if ((z.x>2)||(z.y>2)||(z.x*z.x+z.y*z.y>4)){
        paint=false;
      }   
      z=power(z,eq_pow)+c;
      i++;
    }    
  } else if (type==1){  //Julia
    z=vec2(pass_Position.x*koeff.x+d.x,pass_Position.y*koeff.y+d.y);  
    while ((i<iterations)&& paint){
      if ((z.x>2)||(z.y>2)||(z.x*z.x+z.y*z.y>4)){
        paint=false;
      }   
      z=power(z,eq_pow)+con;
      i++;
    }    
  }
    switch(color_scheme){
    case 0:if (paint) {
            out_Color=inFracColor0();
        } else {
            out_Color=outFracColor0(i);
        };
        break;
    case 1:
        if (paint) {
            out_Color=inFracColor1();
        } else {
            out_Color=outFracColor1(i);
        };
        break;
    case 2:
        if (paint) {
            out_Color=inFracColor2();
        } else {
            out_Color=outFracColor2(i);
        };
        break;
    default: out_Color=vec4(0,0,0,1);
        break;    
    }
  //mapping test:
  //if (distance(vec2(pass_Position.x*koeff.x+d.x,pass_Position.y*koeff.y+d.y),vec2(1f,1f))<0.01){
  //  out_Color=vec4(1,0,0,1);
  //}
}