package ru.bel_riose.fractal_shaders;

import java.util.EnumMap;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Vector2f;



/*ПРИ КОПИРОВАНИИ ПРОЕКТА ДЛЯ СОЗДАНИЯ НОВОГО:
 *1) Сменить имя папки и проекта
 *2)Сменить имена пакетов
 *3)Указать в netbeans в свойствах проекта main class 
 *4)Сменить ${main.class} в pom.xml
 *5)Сменить имя jar-with-dependencies в scripts/run.bat
 */
class FViewer{ 
    public static enum ColorScheme{
        BLACK_WHITE,GRAY,COLOR
    }
    
    public FViewer(GlRenrerer renderer) {
      this.renderer=renderer;
      fullReset();
    }
    GlRenrerer renderer;
    
    ColorScheme colorScheme=ColorScheme.BLACK_WHITE;
    
    float defaultStepCon = 0.01f;
    float stepCon;    
    Vector2f con;  
    
    Vector2f koeff;
    private Vector2f defaultkoeff = new Vector2f(1/0.65f, 1/0.65f);
    
    Vector2f d;
    private Vector2f defaultd = new Vector2f(0, 0);
    
    public void resetCamera(){
      koeff=new Vector2f(defaultkoeff);
      d=new Vector2f(defaultd);
      stepCon=defaultStepCon;
      koeff.x = koeff.x * renderer.size.x / renderer.size.y;
    }
    
    public void fullReset(){
      resetCamera();
      con=new Vector2f(0,0);
    }
}

public class App {
    // Entry point for the application

    public static void main(String[] args) {
        new App();
    }
    
    private final String WINDOW_TITLE = "GLSL Fractal Painter";
    
    private GlRenrerer renderer;
    private float stepK = 0.1f;
    private boolean dragging = false;
    private int dragPrevX = 0, dragPrevY = 0;
    private int defaultIterations = 1000;
    private int iterations = defaultIterations;
    private int power=2;    
    
    
    private EnumMap<FractalTypes,FViewer> fviewers = new EnumMap<FractalTypes, FViewer>(FractalTypes.class);
        
    public static enum FractalTypes {

        MANDELBROT, JULIA
    }
    
    private FractalTypes type = FractalTypes.MANDELBROT;
    private FViewer currentViewer;

    private Vector2f scrToMath(Vector2f v, FViewer camera) {
        return new Vector2f((v.x * 2 / renderer.size.x - 1) * camera.koeff.x + camera.d.x, (v.y * 2 / renderer.size.y - 1) * camera.koeff.y + camera.d.y);
    }
    private Vector2f scrToSquare(Vector2f v) {
        return new Vector2f(v.x * 2 / renderer.size.x - 1, v.y * 2 / renderer.size.y - 1);
    }
    
    void setType(FractalTypes type){
       this.type=type;
       currentViewer=fviewers.get(type);
    }
    
    public App() {
        renderer = new GlRenrerer(WINDOW_TITLE);
        fviewers.put(FractalTypes.MANDELBROT, new FViewer(renderer));
        fviewers.put(FractalTypes.JULIA, new FViewer(renderer));
        setType(type);
        
        boolean running = true;

        while (running) {
            // Do a single loop (logic/render)
            renderer.render(type,iterations, power, currentViewer);

            boolean justPressed = false;
            while (Keyboard.next() && running) {
                int key = Keyboard.getEventKey();
                if (Keyboard.getEventKeyState()) {
                    switch (key) {
                        case Keyboard.KEY_ESCAPE:
                            running = false;
                            break;
                        case Keyboard.KEY_SUBTRACT: //Numpad minus                    
                            currentViewer.stepCon*=5f;
                            break;
                        case Keyboard.KEY_ADD:  //Numpag plus
                            currentViewer.stepCon*=0.2f;
                            break;
                        case Keyboard.KEY_PRIOR: //Page_Up
                            iterations+=100;
                            break;
                        case Keyboard.KEY_NEXT: //Page_Down
                            iterations-=100;
                            break;
                        case Keyboard.KEY_HOME:
                            iterations=defaultIterations;
                            break;                         
                        case Keyboard.KEY_LEFT:
                            currentViewer.con.x -= currentViewer.stepCon;
                            justPressed = true;
                            break;
                        case Keyboard.KEY_RIGHT:
                            currentViewer.con.x += currentViewer.stepCon;
                            justPressed = true;
                            break;
                        case Keyboard.KEY_UP:
                            currentViewer.con.y += currentViewer.stepCon;
                            justPressed = true;
                            break;
                        case Keyboard.KEY_DOWN:
                            currentViewer.con.y -= currentViewer.stepCon;
                            justPressed = true;
                            break;
                        case Keyboard.KEY_R:
                            currentViewer.fullReset();
                            iterations=defaultIterations;
                            break;
                        case Keyboard.KEY_BACK:
                            if (type == FractalTypes.MANDELBROT) {
                                setType(FractalTypes.JULIA);                                
                            } else {
                                setType(FractalTypes.MANDELBROT);
                                currentViewer.con=new Vector2f(0,0);
                                
                            }
                            break;
                        case Keyboard.KEY_TAB:
                            //Cycling through schemes
                            currentViewer.colorScheme=FViewer.ColorScheme.values()[((currentViewer.colorScheme.ordinal()+1) % FViewer.ColorScheme.values().length)];
                            break;
                    }
                }
            }

            if (!justPressed) {
                if (Keyboard.isKeyDown(Keyboard.KEY_LEFT)) {
                    currentViewer.con.x -= currentViewer.stepCon;
                }
                if (Keyboard.isKeyDown(Keyboard.KEY_RIGHT)) {
                    currentViewer.con.x += currentViewer.stepCon;
                }
                if (Keyboard.isKeyDown(Keyboard.KEY_UP)) {
                    currentViewer.con.y += currentViewer.stepCon;
                }
                if (Keyboard.isKeyDown(Keyboard.KEY_DOWN)) {
                    currentViewer.con.y -= currentViewer.stepCon;
                }
            }

            while (Mouse.next()) {
                switch (Mouse.getEventButton()) {
                    case 0:
                        if (Mouse.getEventButtonState()) {
                            if (type== FractalTypes.MANDELBROT){                              
                              setType(FractalTypes.JULIA);
                              currentViewer.resetCamera();
                              currentViewer.con=scrToMath(new Vector2f(Mouse.getEventX(), Mouse.getEventY()),fviewers.get(FractalTypes.MANDELBROT));                              
                            }
                        }
                        break;
                    case 1:
                        if (Mouse.getEventButtonState()) {
                            dragging = true;
                            dragPrevX = Mouse.getEventX();
                            dragPrevY = Mouse.getEventY();
                        } else {
                            dragging = false;
                        }
                        break;
                }
            }
            if (dragging) {
                currentViewer.d.x -= (float) (Mouse.getX() - dragPrevX) * 2 * currentViewer.koeff.x / renderer.size.x;
                currentViewer.d.y -= (float) (Mouse.getY() - dragPrevY) * 2 * currentViewer.koeff.y / renderer.size.y;
                dragPrevX = Mouse.getX();
                dragPrevY = Mouse.getY();
            }
            int scroll = Mouse.getDWheel();
            if (scroll != 0) {
                Vector2f newkoeff = new Vector2f(currentViewer.koeff.x * (1 - stepK * (float) (scroll / 120)), currentViewer.koeff.y * (1 - stepK * (float) (scroll / 120)));
                Vector2f mathMouseCoord = scrToMath(new Vector2f(Mouse.getX(), Mouse.getY()),currentViewer);
                currentViewer.d = new Vector2f(-(mathMouseCoord.x - currentViewer.d.x) * newkoeff.x / currentViewer.koeff.x + mathMouseCoord.x, -(mathMouseCoord.y - currentViewer.d.y) * newkoeff.y / currentViewer.koeff.y + mathMouseCoord.y);
                currentViewer.koeff = newkoeff;
            }
            running = running && !Display.isCloseRequested();
        }

        // Destroy OpenGL (Display)
        renderer.destroyOpenGL();
    }
}
